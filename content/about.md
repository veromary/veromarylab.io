---
title: About us
subtitle: Brandts en berg
comments: false
date: 2019-03-25
---

This somewhat aimless website seemed like a good idea way back when “id.au” domains were new.

For the last decade it has seen many new facelifts, each time accumulating more odds and ends.

Here is a list of some odds and ends:

* [Roman Compline](https://compline.brandt.id.au) (was brandt.id.au/roman-compline)
* [Little Office of the Blessed Virgin Mary](https://littleoffice.brandt.id.au/) - working on recordings for each hour...
* [New Book of Old Hymns](https://newbookoldhymns.brandt.id.au)
* [Roman Martyrology](https://roman-martyrology.brandt.id.au)
* [Epiphany Proclamation](https://brandt.id.au/music/epiphany.html)

The Brandts are currently a family of eight with two budgies and one guinea pig.


