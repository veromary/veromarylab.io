---
date: 2013-12-17
author: veronica
layout: post
title: Guinea Pigs
categories: ["Photos"]
image: snowball400x250.jpg
comments: true
---

As if we didn’t already have enough cute going on around here.

![Elizabeth](/photos/cute.jpg "Elizabeth")

Boys bought two guinea pigs from our local homeschool market day.

![Two guinea pigs](/photos/guineapigs.jpg "Two guinea pigs")

One we call Snowball or Lily.

![Snowball](/photos/snowball.jpg "Snowball")

The other is called Phoebe.

![Phoebe](/photos/phoebe.jpg "Phoebe")

And they are very cute.

Snowball is steadily growing rounder and rounder and seems to be
carrying bonus guinea pigs.

Also, maybe just a side effect of parental inability to find the right
name at the right time, but the name **guinea pigs** seems inept to
describe these diminutive fluff balls. So they are also called
**bunnies** and **pigeons**.

