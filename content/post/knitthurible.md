---
title: "Knit thurible"
author: veronica
date: 2019-07-04T08:11:24+10:00
publishdate: 2019-07-04T08:11:24+10:00
image: "images/shortthuribleclosedsquare.jpg"
categories: ["Craft"]
comments: true
---

Knitted in the round in gold 8 ply with a charcoal briquette in black.

![Flying Saucer Thurible Open](shortthuribleopen.jpg)

This pattern used to be available through Ravelry, but since their [No Trump](https://www.ravelry.com/content/no-trump) policy came in, I have wanted to distance myself from that platform. Not that everything the President of the US says or does is great, more that I am alarmed that a knitting website could move to ban any support of their country's own president.

![Flying Saucer Thurible Hanging](shortthuriblehanging.jpg)

It's good to come back to the idea to make the ultimate knitted thurible.

[Download the PDF](flyingsaucerthurible.pdf)

You can also see my much earlier [crocheted thurible](/thurible.html)

Another designer has unvented a [different crocheted censer](https://www.etsy.com/au/listing/225639836/pattern-for-crocheted-toy-censer-pdf) formed around a fowm hemisphere. They have some lovely Orthodox stuff in their Etsy shop.


