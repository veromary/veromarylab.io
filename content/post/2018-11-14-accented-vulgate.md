---
date: 2018-11-14
title: Accented Vulgate Bible - Genesis
layout: post
categories: ["Education"]
tags: ["Latin"]
image: lasaintebible.jpg
comments: true
---

When typing up booklets I often forget the accent marks. I take it for granted now that after years of reading Latin I can guess how most words are pronounced - especially with parts of the Mass, where there is a lot of repetition over the years.

Having accent marks is great for learning Latin. You learn more thoroughly when you can speak and hear the text. Those little accent marks take the guesswork out of which syllable to land on. Trying to speak each syllable equally is monotonous and loses the rhythm.

Strangely enough, there doesn't appear to be a digital copy of the Vulgate online with accent marks! There is a scanned in [polyglot bible](https://archive.org/details/lasaintebiblepol05vigo/page/n99) but you can't cut and paste very easily there.

So, I took the first reading from the Easter Vigil - Genesis 1:1-2:2 - and put the accents in. As it looked a bit confusing, I made it into a little 12 page booklet - thinking I could illustrate it, or encourage my kids to illustrate it. Then I thought it would make a good video, so made some slides.

A few hours work, and it still looks fairly rough and ready, but I'll post what I have here.

The booklet:  [Genesis1.pdf](/docs/Genesis1.pdf) and [arranged for booklet printing](/docs/Genesis1-book.pdf)

[Genesis2.pdf](/docs/Genesis2.pdf) and [arranged for booklet printing](/docs/Genesis2-book.pdf)

And here is the video of the slideshow:

{{< myoutube epjKJdi3h5g >}}

