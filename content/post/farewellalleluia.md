---
title: "Farewell Alleluia"
date: 2021-01-26T08:51:57+11:00
publishdate: 2021-01-26T08:51:57+11:00
image: "alleluia.jpg"
categories: ["Music"]
comments: true
---

Late last year I thought I'd make some resources for each Sunday's Latin Gospel reading. It's been fun. It does tend to make me confused though, as I am often thinking about the reading for the following Sunday. Thus I somehow thought last Sunday was Septuagesima.

This has been good though, as I have been able to think about the Farewell to Alleluia before it disappears.

Many years ago, I wrote [a short article for Corpus Christi Watershed on this Farewell](https://www.ccwatershed.org/2014/02/15/farewell-alleluia/). At the bottom, the editor Mr Jeff Ostrowski left this cryptic comment.

> **Editor’s Note** • There is actually an entire (liturgical) hymn which says “farewell” to the Alleluia. The Campion Missal cites a few verses in a very subtle way. Here’s a challenge: Did anyone notice which page? Which page number? Let us know by using the CONTACT tab at the top.

I totally missed this at the time - or forgot about it - but coming back now I had to solve the puzzle. I'll put the page number in the footnotes to give you time to figure it out.

So, there's this hymn in Latin:

> Alleluia, dulce carmen,  
> Vox perennis gaudii,  
> Alleluia vox suavis,  
> Est choris cælestibus,  
> Quem canunt, Dei manentes  
> In domo per sæcula.  
>   
> Alleluia læta, mater  
> Concinis Ierusalem,  
> Alleluia vox tuorum  
> Civium gaudentium :  
> Exules nos flere cogunt  
> Babylonis flumina.  
>   
> Alleluia non meremur  
> Nunc perenne psallere,  
> Alleluia nos reatus  
> Cogit intermittere,  
> Tempus instat, quo peracta  
> Lugeamus crimina.  
>   
> Unde laudando precamur  
> Te beata Trinitas,  
> Ut tuum nobis videre  
> Pascha des in æthere,  
> Quo tibi læti canamus  
> Alleluia iugiter. Amen.  
>   

This was translated by John Mason Neale in 1851 as:
  
> Alleluia, song of sweetness,  
> Voice of joy, eternal lay;  
> Alleluia is the anthem  
> Of the Choirs in Heav’nly day,  
> Which the Angels sing, abiding  
> In the House of God alway.  
>   
> Alleluia thou resoundest,  
> Salem, Mother ever blest;  
> Alleluias without ending  
> Fit yon place of gladsome rest;  
> Exiles we, by Babel’s waters  
> Sit in bondage and distress’d.  
>   
> Alleluia we deserve not  
> Here to chant forevermore:  
> Alleluia our transgressions  
> Make us for a while give o’er,  
> For the holy time is coming  
> Bidding us our sins deplore.  
>   
> Trinity of endless glory,  
> Hear Thy people as they cry;  
> Grant us all to keep Thy Easter  
> In our Home beyond the sky,  
> There to Thee our Alleluia  
> Singing everlastingly. Amen.  
>   

There is a modern tune called [Dulce Carmen](https://www.smallchurchmusic.com/Song_Display-New.php?SID=3329) which goes really well with the English. However, being a Gregorian chant nut, I had to look for the chant for the Latin.

I have a copy of J M Neale's book [The Hymnal Noted](https://andrewespress.com/john-mason-neale-the-hymnal-noted/) which has the chant tunes paired with the English texts. So after all his painstaking work translating Latin into English, here I am lifting the old melodies and putting them back with the Latin text.

And here are the results:

[The PDF - two tunes](main.pdf)

[![Alleluia, Dulce Carmen - reunited with the chant](Alleluia-chant.png)](main.pdf)

{{< youtube jtbXcy13_pQ >}}
