---
date: 2013-02-14
author: veronica
layout: post
categories: ["Photos"]
title: Baby Elizabeth
image: elizabeth400x250.jpg
comments: true
---

Good News! We have a baby girl!

<!--more-->

![Elizabeth and dad](/photos/elizabeth-dad.jpg)


![Elizabeth and 3 brothers](/photos/elizabeth-3bros.jpg)

![Bub bub](/photos/elizabethbubbub.jpg)

![Elizabeth with Anthony](/photos/elizabeth-anthony.jpg)


![Elizabeth and Bernard](/photos/elizabeth-bernard.jpg)



![Elizabeth and Granddad](/photos/elizabeth-granddad.jpg)


