---
date: 2013-11-27
author: veronica
layout: post
title: Chesterbelloc copywork
categories: ["Education"]
comments: true
image: thelionhb.jpg
tags: ["handwriting"]
---

Here are some ideas for copywork from Project Gutenberg. Each link takes
you to the ebook page. The HTML format is good for cutting and pasting
into a word processor (like OpenOffice Writer or Microsoft Word). Then
change the font to something suitable to trace, like
[Jarman](http://www.fontspace.com/christopher-jarman) or
[Briem](http://briem.net)

### by Hillaire Belloc:

-   [Bad Child's Book of Beasts](http://www.gutenberg.org/ebooks/27175)
-   [A Moral Alphabet](http://www.gutenberg.org/ebooks/40134)
-   [Cautionary Tales for Children](http://www.gutenberg.org/ebooks/27424)
-   [More Beasts (For Worse Children)](http://www.gutenberg.org/ebooks/27176)

### by Gilbert Keith Chesterton:

-   [The Wild Knight and Other Poems](http://www.gutenberg.org/ebooks/12037)
-   [Poems](http://www.gutenberg.org/ebooks/31184)
-   [Wine, Water, and Song](http://www.gutenberg.org/ebooks/35115)
-   [Greybeards at Play: Literature and Art for Old Gentlemen](http://www.gutenberg.org/ebooks/14706)

### Other works worth mentioning:

-   [A Book of Nonsense by Edward Lear](http://www.gutenberg.org/ebooks/13646)
-   [A Child's Garden of Verses by Robert Louis Stevenson](http://www.gutenberg.org/ebooks/25609)

And here are some I prepared earlier:

{{< selz 1rMKxAw >}}



