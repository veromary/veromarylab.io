---
title: "Cute Lil' Green Alien OLPC XO-4 Laptops"
author: veronica
date: 2020-01-20T18:59:07+11:00
publishdate: 2020-01-20T18:59:07+11:00
image: "xo-4x3.jpg"
comments: true
---

Low-cost, portable, rugged computers have always had a place close to my heart. Frugally making do with the most pared down specs, or maybe just saving money as per my Scottish and Jewish roots.

Back when I was editing [A New Book of Old Hymns](https://newbookoldhymns.brandt.id.au) I had somewhere between 2-4 small boys to raise while editing many lines of OpusTeX code like this:

```
\setgregorian1
\setclef13
\raisesong3\Internote
\musicindent14mm
\initiumgregorianum
\znotes\fissum{1pt}\bmolle b\en
\musicinitial{V}{A}%
\sgn {}{}D\punctum M\egn
\sgn {}{\'o}r\punctum a\egn
\sgn {}o{}\punctum c\egn
\spatium
\begingroup
\bgenerale
\sgn te{}\engl{\idx{Godhead here in hiding},}\punctumcavum c\egn
\spatium
\sgn dev\punctum c\egn
\sgn {}{\'o}t\pes cd\egn
\sgn {}e,\punctum c\augmentum d\egn
\spatium
\divisiominima
\spatium
```

If you can decode some of that, then you might see that that represents just the first few syllables of Adoro Te Devote. 92 pages of similar music meant lots of data entry.

At the time I had a little [Atari Portfolio](http://www.oldcomputers.net/portfolio.html), the world's first palm-top PC. With the aid of a homemade null-modem cable I could transfer files via the serial port of my computer and edit them outdoors. The battery life was great and the monochrome screen easy to read in sunlight.

So when I saw these little green [OLPC](http://wiki.laptop.org/go/XO-4) laptops for sale, I thought they would have to be an improvement on the Atari Portfolio. And they are. But that's part of the problem.

They present many more possibilities and yet they are crippled in weird ways.

 * To Keep Children Safe - many web pages inaccessible
 * To Make Them "User Friendly" - the Sugar desktop thing - especially the Journal as a Filesystem Alternative
 * To Make Them Cheap Enough - compromises with proprietary components requiring proprietary drivers

So, it's not possible to just install a regular OS - you're stuck with Fedora 18 which dates back to 2013. Software tends to change a lot over time. So you can expect things to be broken.

### Getting a web browser to work

I installed Seamonkey which is based on Firefox and works a little better than the pre-installed Epiphany browser.

I've modified about:config (type about:config in the url bar and click through the warning message about any warranty)

Search for `security.tls.version.max` and modify it to **3**

### My Plans

As earlier this century, I have little kids and a whole lot of ideas for books of Gregorian chant. To be make the Little Green Aliens useful I require:

 * A good text editor (vim)
 * git
 * A current version of TeX
 * Gregorio

**Vim** is easy to install. You may prefer Emacs. I'll still talk to you.

```
sudo yum install vim
```

**Git** is also easy to install, but has the drawback of not being able to reach gitlab due to ssl type handshake things and other things being set in 2013. I think.

So, I can work around that by setting up a mirror repository elsewhere on the local area network (Hello Raspberry Pi!) and clone that instead. I still need to tell that mirror to push changes back to the gitlab repo, but still, it's a quantum leap from the Atari Portfolio.

It's possible that I could use rsync instead, but I'm more familiar with git, so I'll stick with that for now.

**TeX** seemed like a formidable giant, until I found [TinyTeX](https://yihui.org/tinytex/) which installs a pared down current portable version. And it works! And I get to learn how to use `tlmgr`. It's also one of the webpages which doesn't work out of the box on the XO-4 and needs Seamonkey with the TLS settings changed as described above.

TinyTeX's installer built to use the /tmp directory during installation, but this /tmp directory is limited to just 50MB on the XO-4 - I have no idea why! So, you need to remount the /tmp partition with a bigger size.

```
sudo mount -o remount,size=500M,noatime /tmp
```

Then follow the instructions from [TinyTeX](https://yihui.org/tinytex/).

Then add /home/olpc/bin to your path:

```
PATH=$PATH:/home/olpc/bin
```

The next step is to make this permanent by adding these lines to your `/home/olpc/.bashrc`

```
PATH=$PATH:/home/olpc/bin:/usr/local/bin

export $PATH
```

(/usr/local/bin is where Gregorio installs, but I ended up putting a symlink to that binary anyway, so feel free to see if it still works without adding that path)

**Gregorio** is hosted on [Github](https://github.com/gregorio-project/gregorio/releases) and this is inaccessible to the little green monsters, so I had to download the latest release and copy across using a USB stick (possibly just needed that TLS trick above). Then the source code needs to be compiled - no precompiled binaries available for this rather unique architecture! This requires a few things to be installed

```
sudo yum install gcc make
```

Then follow the instructions in [Installing Gregorio](https://gregorio-project.github.io/installation.html). Until you get to the error message about sudo not being able to find kpsewhich. Then go add the new TinyTeX path to sudo thus:

```
sudo visudo
```

Then look for a line like

```
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:
```

and add :/home/olpc/bin to the end, like you did with the PATH thing before.

Then return to the Gregorio instructions where you left off (probably `sudo install.sh`)

Then there's an error about not finding texhash - which should be there - but isn't. This is an ongoing mystery, but some research suggests that `mktexlsr`, which is present, is equivalent to `texhash`.

Further problems ensue since gregorio wants to install as root, so executes texhash as root, which sets all the ls-R files as owned by root, so making it impossible for `tlmgr` to update the ls-R files when it runs as olpc. Which all sounds confusing, so after you install gregorio, you may need to set those permissions back to olpc thus:

```
sudo chown -R olpc:olpc .TinyTeX texmf-local
```

Now, while the tmp file is bigger, you might like to install some more tex packages:

```
tlmgr install luatexbase xcolor libertine parallel epstopdf-pkg hyphen-latin
```

and there are probably lots more that I can't remember off the top of my head. Whenever you hit an error compiling things.

Now, when you finally come to compile some documents including gregorio scores, lualatex is going to look for an executable called `gregorio`, but unfortunately the executable is installed as `gregorio-5_2_1` or something like that, so you need to put in a Symbolic Link thus:

```
ln -s /usr/local/bin/gregorio-5_2_1 ~/bin/gregorio
```

After all that, maybe you're happily compiling Gregorio scores now, enjoying the slow, stately pace at which the tiny processor bravely attacks each step of the way.

Next step is to verify if the compilation is accurate.

At first I thought there were differences, but later work showed this only signified a need for recompiling. For a certain complex project I am working on, compiling on the XO-4 takes 26 minutes. On my new laptop this same task is only 1 minute and 44 seconds.

So, can I use this?

Maybe.

The OLPC XO-4 has much better battery life than my proper laptop. It also stays much cooler and is much more robust and portable. It looks like I could feasibly work on it, with the proviso that more proof reading would be needed once it goes back to a stronger computer for making a final copy.

Overall I'm happy. It's not perfect, but it's closer than I hoped. I don't know who would read this article, as it seems like a very niche area, but I would love to hear from any other TeXnicians working on OLPC machines. Or anyone who has the patience to read this far!

----

UPDATE (20/2/2020): The wifi tends to drop out inexplicably over time, which is sort of a blessing in disguise. If you really want to access the internet you can reboot, but in the meantime, there are lots of local repositories on hand which can all do with some updates.

I think I'm putting anyone else off using TeX with all these strange hurdles. I am migrating other computers away from Linux distribution based texlive to TinyTeX. The PPA for texlive 2017 stopped working on my linuxmint desktop and the native version was only 2015, so I've jumped ship and am enjoying getting TinyTeX up to speed. Tonight I managed to get pdfbook2 working by installing more packages.

UPDATE (21/6/2020): Just settling into a new secondhand laptop with decent battery life and appreciating these instructions for setting up TinyTex with gregoriotex. It could be interesting to see how much time I spend installing stuff into new secondhand laptops. Could I justify getting a fancy [Purism laptop](https://puri.sm/products/librem-13/)? Probably not. And I'm quite happy with the current laptop (i5, 7th gen, 8GB RAM, dual booting Win 10 / Lubuntu 20.04)

Still, the XO-4 is better for outdoor work. And my littlies use them on car trips - so hopefully I'll get the spare time to put together some useful games for learning Gregorian chant and hymns and Useful Stuff.

