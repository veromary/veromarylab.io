---
title: "Annunciation 2nd Vespers"
date: 2020-03-25T14:12:23+11:00
publishdate: 2020-03-25T14:12:23+11:00
image: "annunciation.jpg"
categories: ["Music"]
comments: true
---

After a few hours unsuccessfully trying to get [Albert Bloomfield's Vespers Book](https://asbloomf.github.io/gabc-chants/) to compile, I gave in and am using his precompiled 1st Vespers handout, with my own ammendation for the Magnificat to make it into a 2nd Vespers plus the Commemoration of the Feria for 2020.

[Behold My 2 A4 Pages](annun2vespers2020.pdf)
 
Which I now realise is redundant, as I could have used a few pages from his complete Vespers book.

Reading through the code for the Vespers Book, I am impressed with how differently projects can be organised. Although he uses the same software, and he has documented his work better than I have, I am still lost. Gregoriotex has changed quite a bit over the last five years. Things which just work now, previously required kludgy workarounds, which now prevent anything working at all.

In case it helps anyone else, here are my files:

 * [main.tex](main.tex)
 * [Magnificat Antiphon in gabc](an--gabriel_angelus..._ave.--solesmes_1961.gabc) - straight from [gregobase.selapa.net](https://gregobase.selapa.net/)
 * [Ferial Antiphon in gabc](an--ille_homo--solesmes.gabc) - also from gregobase
 * [Finished pdf](annun2vespers2020.pdf)
 * [Magnificat Verses](Magnificat-solemn7d.gabc) from [bbloomf.github.io/jgabc](http://bbloomf.github.io/jgabc/psalmtone.html#editor)

So, we have all the tools with which to pray the Divine Office. Let's get praying!

Also worth a mention is Jeff Ostrowski's [Draft Vespers Booklet](https://www.ccwatershed.org/2018/08/07/pdf-download-sunday-vespers/) which gives the basic Sunday Vespers in a most user-friendly format.

And if you can't get it to come together, there's still the Rosary, the Divine Mercy Chaplet, the Jesus Prayer...

[Compline](https://compline.brandt.id.au) is a nice way to ease into Vespers too.

