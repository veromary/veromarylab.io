---
title: "2018 Plans"
date: 2018-01-26T10:25:43+11:00
image: tealbyspa400x250.jpg
categories: ["Education"]
comments: true
---

Each time I come to write one of these annual plans I am struck by how time flies. This year I have been looking at years 11, 9, 7, 5 and Kindergarten. How did this happen?

All the same, I am aware of how arbitrary some of the choices we make seem.

I remember long ago fixing my goal as twofold:

 * You don't know everything
 * You can learn anything

It's finding the sane balance between presumption and despair.

So, this year - we've signed up for IXL subscriptions - a mum on the Homeschool Australia Facebook group arranged a bulk subscription. It covers Mathematics and English from the Australian Curriculum and provides a handy table of data to gauge how they're going.

My eldest is enrolled in the [Augustine Academy](https://augustineacademy.com.au).

We have ongoing plans for Science Experiments, Bushwalks, Technical Drawing lessons with Dad, Music, Automotive Technology with Dad, learning how to use Git, Australian History and probably more stuff. I'm continuing with Italian and Latin and Music for myself - if anyone wants to join me, they're warmly welcome!

We expect a visit from the NSW Education Standards Authority at the end of the year, so hope to keep records covering all the Key Learning Areas. There's always that pull between satisfying the standards of our local Educational Authorities and our own ideas of a well rounded education.

It's still a roller coaster - still seems amazing that we're hanging in there - still hoping that we'll find more like-minded friends, mentors, helpers along the way.
