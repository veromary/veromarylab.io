---
title: "Epiphany Proclamation 2020"
date: 2019-12-09T12:05:44+11:00
publishdate: 2019-12-09T12:05:44+11:00
image: adoration_of_the_magi_tapestry400.jpg
categories: ["Music"]
comments: true
author: veronica
---

It's that time of year again. Since 2003 every year someone has requested that I put together the Proclamation of Moveable Feasts for the feast of the Epiphany. My local parish / apostolate doesn't use it, so I don't get to hear it sung, but it is reassuring that around the world people still sing it. 

It's up and ready at [Music > Epiphany Proclamation](https://www.brandt.id.au/music/epiphany.html)

If you would like to leave a donation to support this work and maybe justify music lessons for my kids and maybe even myself, I have [this PayPal Link](https://www.paypal.me/veronicabrandt) or you can [buy my book](https://newbookoldhymns.brandt.id.au).


