---
title: "Little Office of the Immaculate Conception"
date: 2020-11-14T18:55:53+11:00
publishdate: 2020-11-14T18:55:53+11:00
image: tealbyspa400x250.jpg
comments: true
---

My most popular videos on Youtube are still those on the Little Office of the Blessed Virgin Mary. I've been asked at least twice about another Little Office, dedicated to Our Lady under the title of her Immaculate Conception.

Michael Martin has presented this devotion [in parallel columns in Latin and English.](https://www.preces-latinae.org/thesaurus/BVM/OPConImm.html)

It follows the idea of the Divine Office, except without the psalms, which seems like having a ham sandwich without the ham, but it does make praying it a whole lot simpler.

It's basically one long hymn, broken up into seven pieces (Matins and Lauds are combined), with one extra piece for Compline. There's a short introductory bunch of versicles to start and each ends with the same collect (prayer). The extra bit of hymn for the end of Compline seems to stand in place of the Marian Antiphon and also concludes with a very short antiphon, a versicle and a collect.

Singing the English version seems straightforward. Once you have learned a tune which fits the hymn, the rest will be mostly simple tones for collects and versicles. The hymn appears in a few hymnbooks, including three that I happen to have prepared reprints of through Lulu:

* [St Basil's Hymnal](https://www.lulu.com/en/us/shop/the-basilian-fathers/st-basils-hymnal-1918/paperback/product-179k9mmv.html)
* [A Treasury of Catholic Song](https://www.lulu.com/en/us/shop/sidney-hurlbut/treasury-of-catholic-song/paperback/product-1epvy529.html)
* [Arundel Hymns](https://www.lulu.com/en/us/shop/charles-gatty-and-henry-duke-of-norfolk/arundel-hymns/paperback/product-1ggn8qww.html)

St Basil's uses a simple tune. [Here is the relevant page from the Internet Archive](https://ia801603.us.archive.org/view_archive.php?archive=/0/items/StBasilsHymnal1918/StBasilsHymnal1918_jp2.zip&file=StBasilsHymnal1918_jp2%2FStBasilsHymnal1918_0107.jp2&ext=jpg). It repeats the last two lines or so of each verse. It's not my favourite, but it's not bad.

Rev Fr Hurlbut sets the hymn to the tune that I know well from O Purest of Creatures, based on the German "Maria zu Lieben". [Here is the page from the Internet Archive](https://ia800202.us.archive.org/view_archive.php?archive=/32/items/treasuryofcathol00hurl/treasuryofcathol00hurl_jp2.zip&file=treasuryofcathol00hurl_jp2%2Ftreasuryofcathol00hurl_0230.jp2&ext=jpg). I really like this one, as it is a tune I am really familiar with and closely associate with thinking about Mary.

Finally, Arundel Hymns pairs the text with the tune known as Hanover, which I have heard sung by Maddy Prior in her album of Gallery Hymns of the 18th and early 19th centuries. She sings the tune with "O Worship the King", but [here is the link if you would like to listen](https://www.youtube.com/watch?v=k1DImOORTAw&list=OLAK5uy_kgLL5SUnBjeeoGs2gPn2vM9_X27LJQcLg&index=15). If you'd like the page from Arundel Hymns, [here it is in tiff format](https://archive.org/download/arundelhymnsand00gattgoog/arundelhymnsand00gattgoog_tif.zip/arundelhymnsand00gattgoog_tif%2Farundelhymnsand00gattgoog_0387.tif).

So, you have a choice of three tunes!

The Latin has a similar meter, so you could use these tunes, but it feels like there ought to be a Gregorian chant way to sing it.

[This Polish website](https://www.sanctamissa.pl/en/prayer-and-hours/officium-de-immaculata-conceptione-b-v-mariae-little-hours-to-the-immaculate-conception-of-the-blessed-virgin-mary/) has sung recordings of the Latin with something of a chant feel to it, but no sign of where their music came from.

There is a book dedicated to Marian Gregorian chant called <i>Cantus Mariales</i>, [lovingly retypeset by Andrew Hinkley](https://github.com/ahinkley/cantus-mariales-1903/blob/master/Cantus_Mariales-main.pdf). It includes a hymn called <i>Salve mundi Domina</i>, but apart from those first words, the rest is different.

So I'm still all at sea when it comes to singing the Latin.

But I might just have enough info to make a Youtube video about singing the English version.

Stay tuned!

