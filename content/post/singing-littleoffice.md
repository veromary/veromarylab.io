---
title: "Singing the Little Office of the Blessed Virgin Mary in Latin"
date: 2020-12-19T07:33:51+11:00
publishdate: 2020-12-19T07:33:51+11:00
image: "udemycover.png"
tags: ["Latin"]
categories: ["Music"]
comments: true
---

**TLDR:** I made a course on Udemy about how to sing the Little Office of the Blessed Virgin Mary. [Check it out!](https://www.udemy.com/course/sing-officium-parvum-bmv/?referralCode=7C9068C18692B9588F76)

Here follows a meandering tale of how this course came to be.

The most popular video I have ever uploaded to Youtube is [a simple recording of Memento Rerum Conditor](https://www.youtube.com/watch?v=VltiRfD16zY). This hymn is sung in most of the hours of the Little Office of the Blessed Virgin Mary.

I've had a vague interest in the Divine Office ever since I learned about it. Back when I was young and single and attending the [Maternal Heart of Mary Church, Lewisham](https://maternalheart.org) Thursday evening Masses were followed by Compline. Back then they used the Roman Compline as described in the Liber Usualis. So, that's what I learned.

When I married Peter we would sing this of an evening. At Lewisham we had only learned the parts for Thursdays and Sundays, so we had to muddle it out from the Liber Usualis, but it went pretty well. Until we had kids.

As you might expect, things change with young kids in the house. It wasn't till some years later that we tried to pick up Compline again. We picked up pieces, rather than diving back into the full thing. Things like "In Manus Tuas, Domine" and the prayer "Visita Quaesumus" came first. My sister-in-law visited - she is Christian and a polyglot, speaking several languages and having studied Latin too - so we finished the day with our mash up Compline. She very tactfully let us know that she couldn't follow what we were singing, which spurred me on to start making booklets for our version of Compline.

Once we had our own booklets, we built up to a fairly complete version of Compline. The current booklets are up at [compline.brandt.id.au](https://compline.brandt.id.au) - we even made recordings for a while there!

Somewhere along the way learned about the Little Office of the Blessed Virgin Mary. I was so keen I bought seven copies of the little Baronius Press books and had six of them stamped with each family member's initials (one spare, which proved insufficient as we had two more babies...) For a while we were singing Compline from the Little Office. I remember impressing some people at our first Christus Rex pilgrimage by singing it from memory with the kids in the car.

I wanted to sing more than just Compline, so picked up Prime from the Little Office. Now we have that pretty much all memorised. I still have to pick up the book for the Collects for Advent and Christmas.

The little blue Baronius Press books weren't so helpful when it came to singing the Psalms, so I made up a booklet for those. Once we had learned the tunes we went back to the little blue books. Or, more accurately, we mostly went from memory except for me picking up a book to check from time to time.

So, then Memento Rerum Conditor hit Youtube and I made a few videos on how to sing the Little Office. The Little Office is a pretty big thing though, and youtube isn't the greatest place for sustained concentration and recollection. Then 2020 came along and choir was out of the question, so instead of preparing music for Sunday Mass I prepared videos for Udemy and made a course.

[Check it out!](https://www.udemy.com/course/sing-officium-parvum-bmv/?referralCode=7C9068C18692B9588F76)

No musical ability required - it's mostly listening to me sing. The pdfs are there. I need to polish them up. It will be an ongoing work in progress for some time, but at least it's a start!
