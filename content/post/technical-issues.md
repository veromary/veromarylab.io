---
title: "Technical Issues"
date: 2018-12-22T18:03:04+11:00
publishdate: 2018-12-22T18:03:04+11:00
image: "githublab.jpg"
categories: ["Computers"]
comments: true
---

UPDATED: 30/3/2020

A few days before Christmas might be a funny time to try migrate from Github Pages over to Gitlab.

Just for the moment, if you're looking for the Roman Martyrology try:

[https://roman-martyrology.brandt.id.au](https://roman-martyrology.brandt.id.au)

If you're looking for A New Book of Old Hymns:

[https://newbookoldhymns.brandt.id.au](https://newbookoldhymns.brandt.id.au)

Little Office

[https://littleoffice.brandt.id.au](https://littleoffice.brandt.id.au)

Roman Compline:

[https://compline.brandt.id.au](https://compline.brandt.id.au)

You might notice a pattern there if there's something I've forgotten - like Orbis Pictus. The old Latin folder has been copied over the catholicslatin at gitlab, but isn't up and running right now.

Hope that helps!
