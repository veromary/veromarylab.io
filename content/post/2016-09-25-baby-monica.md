---
categories: ["Photos"]
author: veronica
date: 2016-09-25
title: Baby Monica
image: monica400x250.jpg
comments: true
---

Sixth baby, second girl, second homebirth, first daytime birth.  A new baby with two teenagers in the family.

![Monica via ultrasound](/photos/monica-ultrasound.jpg)

![Monica with mum and dad](/photos/monica-mumdad.jpg)

![An Early Feed](/photos/monica-earlyfeed.jpg)


![Baptism](/photos/monica-baptism.jpg)

![with mum](/photos/monica-mumbaptism.jpg)

![Helping Nanna play Scrabble](/photos/monica-nanscrabble.jpg)

![Monica and Elizabeth](/photos/monica-elizabeth.jpg)

