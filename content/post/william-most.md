---
title: "Latin by the Natural Method by William Most"
date: 2019-06-28T09:30:48+10:00
publishdate: 2019-06-28T09:30:48+10:00
image: "latin_natural_method_vol1.jpg"
categories: ["Education"]
comments: true
---

I found a new website: [Learn Church Latin](https://learnchurchlatin.com/)

It's by a Catholic homeschooling mum like me, who, unlike me, has done actual proper Latin courses at actual proper educational institutions! I am stoked!

She recently wrote about a [Natural Latin Method written by a priest, Fr William Most](https://learnchurchlatin.com/2019/06/21/latin-by-the-natural-method-vol-1-fr-william-most/). It sounds fantastic! So far my kids have been chuckling over the first two lessons. *María habuit agnum parvum. María non fuit agnum parvum. Agnus dixt: baaa, baaa.*

The downside is that it seems to require the guidance of a fairly knowledgable teacher to get the most out of the course. The Teachers Manual is aimed at Teachers. A really self-motivated student could get a lot out of it, but it's far from a set and forget high school text. But really, it takes a very motivated student to work through any text independently. Ah, homeschooling :)

Here are the key links:

[Free PDFs of 1964 edition textbooks](https://cerclelatin.org/wiki/Most_version_anglaise/second-year) plus scripts for practise tapes - more audio practice sounds great. The PDF textbooks are somewhere between Letter and A4 page size. You could print out a few lessons at a time to minimize the intimidation factor.

[Reprints from Mediatrix Press](http://mediatrixpress.com/?p=356) - they are also working on reproducing the Audio Practice Tapes.

[Other religious tracts by Fr William Most](https://www.catholicculture.org/culture/library/most/browse.cfm)


