---
date: 2014-08-07
author: veronica
title: Melbourne 2014
categories: ["Photos"]
image: melbo11400x250.jpg
---

Most of us went to Melbourne again.

I went to a breastfeeding conference with another breastfeeding enthusiast.

<img src='/photos/lizbeth140802.jpg' alt="Lizabeth up the back of the theatre">

The rest of the crew did various touristy things.

<img src='/photos/werribeegiraffe.jpg' alt="Taking photos at Werribee">

<img src='/photos/gorilla.jpg' alt="Gorilla">

<img src='/photos/werribeemeerkat.jpg' alt="Meer kat">

<img src='/photos/werribeemonkeys.jpg' alt="Monkeys">

