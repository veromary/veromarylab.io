---
date: 2014-12-02
author: veronica
title: Epiphany Proclamation 2015
categories: ["Music"]
image: adoration_of_the_magi_tapestry400.jpg
comments: true
---

It's up and ready at [Music > Epiphany Proclamation](http://www.brandt.id.au/music/epiphany.html)

This year I had some feedback from Indonesia and had a look around for more about the proclamation of moveable feasts.

The Vatican has all their liturgy booklets available [through their Calendar of Celebrations](http://www.vatican.va/news_services/liturgy/calendar/ns_liturgy_calendar_en.html).  So you can see the words they use each Epiphany.

The template for how to work the tune is in a book called Laudes Festivae (1940): Christmas proclamation, from the Roman Martyrology, the sung Gospel for Mass on solemn feast days, the sung lessons of Triduum vigils (Tenebrae) available through [Musica Sacra's Latin Chant collection](http://musicasacra.com/music/latin-settings/).

I started typing these for the Maternal Heart of Mary starting Epiphany 2003.  I was going from handwritten notes from our local choirmaster and our local liturgical expert.  Each year after Midnight Mass I would be greeted with those dreaded words "Did you get my email?" and each year would try remember how I put it together 12 months ago and knock out a new version.

You can see the collection [here](http://www.brandt.id.au/music/epiphany.html)

I can make modifications for you - or better still, download the gabc and modify it online with the [Illuminare Score Editor](http://dev.illuminarepublications.com/gregorio/).

