---
title: Classes
subtitle: Workshops with Veronica Brandt
comments: false
date: 2021-04-01
---

Online class on Singing the Little Office available via Udemy:

[Singing the Little Office of the Blessed Virgin Mary](https://www.udemy.com/course/sing-officium-parvum-bmv/?referralCode=7C9068C18692B9588F76)

## Singing with Veronica Brandt

#### One-day workshops 9am-3pm

We will cover:
  * Basics of voice production, breath control, placement
  * Pitch awareness, introducing solfege
  * Exploring rhythm through body percussion
  * Singing melodies together
  * Introducing harmonies through simple rounds

Music is drawn from traditional Australian songs, folk songs and medieval sacred music. Students get to take home booklets of the music included.

The day finishes with a little performance of what we have learned.

Workshops take place in my home. Parents are encouraged to attend with their kids. Please bring a packed lunch. 

No previous singing experience necessary.

Cost for the day is $100 per student.

I am a registered Creative Kids Provider.

#### Outcomes:

Students will :
  * improve their awareness of how their voices work
  * sing simple melodies
  * learn to describe pitch with solfege and intervals
  * gain more confidence in performing
  * co-operate with people

#### Plan:

 * Start with rhythmic movements, marching, clapping, tapping
 * Making vowel sounds using their upper register
 * Listening to the music we will be learning
 * Break for morning tea
 * Introduce solfege syllables
 * Revisit the piece of music, tapping out the rhythm
 * Sing the tune with solfege
 * Sing the tune with words
 * A game exploring dynamics (loud/soft)
 * Singing a more well known piece of music
 * Break for lunch
 * Rehearsing the new piece
 * Setting up for the performance
 * Performance
 * Thank yous and close



