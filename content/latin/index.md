---
title: Learning Latin
type: latin
page: latin/list.html
comments: true
tags: ["Latin"]
categories: ["Education"]
image: images/clicover.jpg
---

I like learning Latin. I like sharing Latin with my kids. As I'm a cheapskate, I like to self-publish books via Lulu:

* [Elements of Latin, by Benjamin D'Ooge](http://www.lulu.com/shop/benjamin-dooge/elements-of-latin/paperback/product-23089844.html) - spiral bound, 462 pages
* [Key to Elements of Latin](http://www.lulu.com/shop/benjamin-dooge/key-to-elements-of-latin/paperback/product-23088778.html) - paperback, 70 pages
* [Catholic's Latin Instructor, by Edward Caswall](http://www.lulu.com/shop/edward-caswall/the-catholics-latin-instructor/paperback/product-18844755.html) - paperback, 282 pages

A few other projects:

* [New Book of Old Hymns](https://newbookoldhymns.brandt.id.au/) - lots of Latin hymns
* [Kids Chant](https://kidschant.com) - teaching kids Gregorian chant and Latin.
* [Ora Maritima](https://veromary.gitlab.io/oramaritima) - a reader with grammar and exercises from 1908
* [Orbis Pictus](https://veromary.github.io/orbispictus/) - a reader from centuries ago - world's first picture book

On to the original aim of this page:

Catholic's Latin Instructor
===========================

<a href="http://www.lulu.com/shop/edward-caswall/the-catholics-latin-instructor/paperback/product-18844755.html"><img src="/images/clicover.jpg" align="right" alt="Catholic's Latin Instructor in paperback" class="align-right"></a>

Fr Edward Caswall was a quiet hero of the Oxford Movement who translated hymns and authored a few of his own too. He also wrote The Catholic’s Latin Instructor which you can buy [as a paperback](http://www.lulu.com/shop/edward-caswall/the-catholics-latin-instructor/paperback/product-18844755.html). It is also available at the [internet archive](http://archive.org/details/thecatholicslati00caswuoft).

The book is a series of exercises consisting of putting together the Latin from the Mass and other popular devotions with their English translations. It does include a short Latin Grammar, but much is just a straight patching of Latin onto English. Sections are designed to cater for different groups of the faithful – one for servers, another for choirs, though there’s nothing to stop you going through all the sections.

Because of this correspondence between the Latin and the English it lends itself to flash card programs like Hot Potatoes, [Quizlet.com](https://quizlet.com/Brandts/folders/catholics-latin-instructor) and Anki. So the plan here is to collect supporting online materials to make this book more useful.

Table of Contents:
------------------

**This section sustained severe injuries in the transition from Jekyll to Hugo. The arcane way Hugo handles sections is still beyond me, so I started pasting into [kidschant.com](https://kidschant.com).**

* [Preface](/latin/preface/)

### Part I

* Chapter 1 – [Benediction](/latin/c1benediction/)
* Chapter 2 – [Mass](/latin/c2mass/)
* Chapter 3 – [Serving at Mass](/latin/c3server)
* Chapter 4 – Various Prayers and Offices
       + Section 1
       + Section 2

### Part II

* A Short Latin Grammar
* First Course [Exercises, Text, and Word-for-word Translation.]
    + Chapter 1 – Sunday Vespers
    + Chapter 2 – Additional Vesper Psalms
    + Chapter 3 – Compline
* Second Course [Text and Word-for-word Translation.]
    + Chapter 1 – Additional Portions of the Mass
    + Chapter 2 – Requiem Mass
    + Chapter 3 – Various Short Devotions
    + Chapter 4 – Select Vesper Antiphons
    + Chapter 5 – Select Hymns and Sequences
* Third Course [Text only.]
    + Chapter 1 – Litany of the Saints
    + Chapter 2 – Select Portions of the Burial Office
    + Chapter 3 – Select Introits
    + Chapter 4 – Words of various Motets
    + Chapter 5 – Select Collects, etc.
    + Chapter 6 – Select Gospels, etc.
    + Chapter 7 – Creed of St Athanasius
    + Chapter 8 – Recordemini

### Little Dictionary

### Index to both parts


# Bible handwriting

Some random pages of handwriting practice using the free fonts Jarman and [Briem](http://briem.net)

  + [Beatitudes pdf](/latin/bible/beatitudes.pdf) (and [editable odt](/latin/bible/beatitudes.odt))
  + [First 3 Commandments pdf](/latin/bible/xcommandments123.pdf) (and [editable odt](/latin/bible/xcommandments123.odt))


