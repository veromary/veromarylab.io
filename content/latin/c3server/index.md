---
type: "latin"
page: "latin/list.html"
title: Server Latin
comments: true
---

# Chapter 3

## Server Latin

The [Catholic's Latin Instructor](/latin/) separates the parts of the Mass for the choir and congregation from the parts of the Mass for the altar servers.

The changes to the Mass around 1970 blurred the old layering of the Mass whereby servers and choir would focus on different prayers. 

It's still good for both groups to know both prayers, but for beginners it's good to have a narrower focus on the vocabulary that will be most useful in each case.

Here is the full list of parts for servers to learn, according to the Instructor:

* Judica Me
* Confiteor
* Versicles
* Responses at Epistle and Gospel
* Orate and Suscipiat
* Commencement of Preface
* Sed libera nos, &c.

We'll keep adding links below as we make online versions of each part.
