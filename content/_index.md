---
title: Home
author: veronica
---

“When we step into the family, by the act of being born, we do step into a world which is incalculable, into a world which has its own strange laws, into a world which could do without us, into a world we have not made. In other words, when we step into the family we step into a fairy-tale.” &mdash; Gilbert Keith Chesterton, “On Certain Modern Writers and the Institution of the Family,” Heretics

